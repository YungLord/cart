var data = [
    {'id':'0','name':'Ice-Cream'},
    {'id':'1','name':'Hot-Dog'},
    {'id':'2','name':'Popcorn'},
    {'id':'3','name':'Cookie'},
    {'id':'4','name':'Bag Juice'}
]
    
    var selectedData= [];
    
    //Display all cart items in the cart wrapper
    function showCart(){
        if(selectedData.length > 2){
            let count = (selectedData.length -2);
            document.getElementById('cartWrapper').innerHTML = selectedData[0] + ', ' + selectedData[1] + " and " + count + " more";
        }else{
        document.getElementById('cartWrapper').innerHTML = selectedData;
    }
}

// This function will add all items to cart or remove all item from cart respectively.
function checkAll(){
    if(selectedData.length != data.length){
        selectedData.splice(0, selectedData.length);
        data.forEach(i=>{
            selectedData.push(i.name); 
            document.getElementById(`${i.id}`).style.background = "#F1F1F1";            
        })
        checkAllHighlight();
    }else{
        selectedData.splice(0, selectedData.length);
        data.forEach(i=>{
           document.getElementById(`${i.id}`).style.background = "none";
        })
        checkAllHighlight();
    }
    showCart();
}

//Highlight check all
function checkAllHighlight(){
    if(selectedData.length === data.length){
        document.getElementById('checkall_id').style.background = "#F1F1F1";
    }else{
        document.getElementById('checkall_id').style.background = "none";
    }
}

// Add selected Items to the Cart.
function addToCart(item) {
    let result = data.find(items => items.name === item);
         
    if (selectedData.includes(result.name)==false){
        selectedData.push(result.name);
        document.getElementById(`${result.id}`).style.background = "#F1F1F1";
        checkAllHighlight();
    }else{
        let index = selectedData.indexOf(result.name);
        selectedData.splice(index, 1);
        document.getElementById(`${result.id}`).style.background = "none";
        checkAllHighlight();
    }
    showCart();
}

//This function will run when program launch.
window.onload =  main=()=>{
    var List = document.getElementById('displayCart')

    data.forEach(item=>{
        var listItem = `
            <ul class="list-item">
                <li id="${item.id}" onclick="addToCart('${item.name}');">${item.name}</li>
            </ul>
        `
        List.innerHTML += listItem      
    })          
}